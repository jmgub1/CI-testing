// A simple unit test just to ensure CI is working
// Uses the mocha framework
var assert = require('assert');
describe('Array', function() {
  describe('#indexOf()', function() {
    it('simple 0=1-1 test for CI testing', function() {
      assert.equal(1-1, 0);
    });
  });
});